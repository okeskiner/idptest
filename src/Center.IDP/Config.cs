﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Center.IDP
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            //Scopeları kapsıyor
            //Bu scopelar ile identitytoken ile ilgili claimler çekilir.  
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),               
                new IdentityResources.Profile(),
                //Eğer kullanıcının adres bilgisini istiyorsak
                new IdentityResources.Address(),
                //Rol diye tanımlı bir identity resource olmadığı için custom Identity Resource ekliyoruz
                new IdentityResource("roles","Your Role(s)",new List<string>(){ "role"})
            };

        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            { 
                new ApiResource("imagegalleryapi","Image Gallery API", new List<string>(){ "role"})
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            { new Client {
                ClientName = "ImageGallery",
                 AllowedScopes =
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    IdentityServerConstants.StandardScopes.Address,
                    "roles",
                    "imagegalleryapi"
                },
                //mvc sitemiz
                ClientId = "imagegalleryclient",
                AllowedGrantTypes = GrantTypes.Code,
                RequirePkce = true,
                
                RedirectUris = new List<string>()
                {
                    //Web sitemize dönüş adresi bu kısmı web tarafında handle edeceğiz
                    "https://localhost:44389/signin-oidc"
                },
                PostLogoutRedirectUris = new List<string>()
                {
                    "https://localhost:44389/signout-callback-oidc"
                },
               
                ClientSecrets =
                {
                    new Secret("secret".Sha256())
                }
            }            };

    }
}