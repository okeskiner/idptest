﻿>  authorization endpoint IDP level redirection endpoint return
> code&token client level token endpoint IDP level
> 
> openId connect flows
> 
> authorization code 
> -tokens from token endpoints
> -confidential clients
> -long lived access
> 
> implicit
> -tokens from authorization endpoint
> -public clients
> -no long lived access
> 
> hybrid tokens from authorization endpoint & token endpoint
> confidential clients long-lived access
> 
> response type flow code - authorization code id_token - implicit
> id_token  token - implicit code id_token - hybrid code token - hybrid
> code id_token token - hybrid the authorization code flow
> authentication request to the authorization flow
> 
> https://idphostaddress/connect/authorize? client_id=imagegalleryclient
> &redirec_uri=https://clientapphostaddress/signin-oidc &scope=openid
> profile &response_type=code &response_mode=form_post
> &nonce=tt34t34t34t
> 
> buradan gelen response ile arka planda token endpointine gidilir
> 
> token request(code,clientId,clientSecret) -> token endpoint
> -> bu istekten id_token gelir bu token valide edilmiştir. Bu elimizdeki id_token ile login olabiliriz

Solution ilk başlangıçta 3 adet projeden oluşuyor. Bir api ,bir mvc ve model sınıflarını tuttuğumuz ayrı model c# class library. Proje dizini daha sonra komut satırından açtık

Komut satırına  `dotnet new -i IdentityServer4.Templates` yazarak identity server templatelerini restore ettik. Daha sonra temel identityserver templati oluşturduk

Komut satırına `dotnet new is4empty -n Center.IDP` isimli projeyi ekledik.

Daha sonra projemizin içinde  `cd Center.IDP`  diyerek girdik. Burada UI kısmı olmadığı için projeye UI kısmını eklemek için komut satırına

`dotnet new is4ui` yazarak ui templateleri projeye ekledik.

Proje içerisinde TestUser.cs içini açıp kendimize test kullanıcısı ekledik. 
**Startup.cs** içinde builder değişkenine `.AddTestUsers(TestUsers.Users);` ekledik. Test kullanıcılarımız geldi. Ayrıca configure içinde bulunan uncomment olan `app.usestaticfiles()` ve  
 

    app.UseIdentityServer();

    // uncomment, if you want to add MVC
    app.UseAuthorization();

açtık. 

Identity Server4 konfigürasyonunu yapmak için **Config.cs** gittik Burada kullanıcıların OIDCleri aktif hale getirmek için scope ekledik

     new IdentityResource[]
                {
    
                    new IdentityResources.OpenId(),
                    //bu scopelarda related claimsleri getiriyorlar
                    new IdentityResources.Profile()
                };

ve aşağıdaki client ekledik

 

    public static IEnumerable<Client> Clients =>
                new Client[]
                { new Client {
                    ClientName = "ImageGallery",
                    //mvc sitemiz
                    ClientId = "imagegalleryclient",
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = new List<string>()
                    {
                        //Web sitemize dönüş adresi bu kısmı web tarafında handle edeceğiz
                        "https://localhost:44389/signin-oidc"
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    },
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    }
                }            };

Bu ayarı yaptıktan sonra mvc projemizin **StartUp.cs** gidip aşağıdaki config ayarlarını ekledik.

      services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                }) //authentication cookie bazlı tutacak ilgili claimleri
                 .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme)
                //buradan client - openid servera bağlanıyor
                .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options => {
                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.Authority = "https://localhost:44318"; //identityserver urli
                    options.ClientId = "imagegalleryclient";
                    options.ResponseType = "code";
                    options.UsePkce = false;
                    //options.CallbackPath = new PathString();
                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.SaveTokens = true;
                    options.ClientSecret = "secret";
                });
			
			
mvc projemizde controller index action içine  `await WriteOutIdentityInformation();` isimli bir method ekledik. Buradan gelen
identitytoken nasıl aldığımızı ve claimleri nasıl listelediğimizi gösterdik.
	
	
09.06.2020 12:19 pkce (proof key code exchange security özelliği aktifleştirildi)
	
 09.06.2020 13:25 logout butonu eklendi. Burada üyemiz logout yaptığı halde anasayfaya dönmek istediğinde logout yapmış olmuyor. Sadece asp.net mvc projesindeki cookie siliniyor. Identityserver tarafında logout yapmış olmuyor. Dolayısıyla user login ekranında beni hatırlayı işaretlediği için otomatik tekrar login olup anasayfaya düşüyor. Eğer login ekranında beni hatırla işaretlemediyse otomatik olarak login olup consent ekranına düşüyor. Bir sonraki fazda identityserver tarafında nasıl logout edilir ona bakacaz  
	
09.06.2020 13:35 client app logout olduktan sonra identity server üzerinden logout olacak fonksiyon eklendi. Ancak user orada kalıyor client app'e yönlenmiyor. Bunu bir sonraki fazda düzelteceğiz
	
09.06.2020 13:43 client app logout olduktan sonra identity server üzerinde logout bilgi ekranını getirdik. Orada clintappa dönmek için bu url linkine tıklayın ibaresi çıkıyor. İşte bu noktada linki tıklayınca client app'a gidiyor ancak o da tekrar bizi identity server login sayfasına atıyor. İşte bu işi otomatikleştirmek için yapacağımız işlem bir sonraki fazda.
	
09.06.2020 13:56 Bu işi otomatikleştirmek için **AccountOptions.cs** içinde bulunan `AutomaticRedirectAfterSignOut = false` ibaresini
`true` olarak değiştireceğiz. Şimdi bize gelen token incelediğimizde içinde openid ve profile scopelarının yer almadığını görüyoruz. Bunun sebebi identity token güvenlik sebebiyle kişisel bilgiler içermez bunu düzenlemek için bir sonraki faza geçiyoruz.
![authorizaation code flow](https://i.imgur.com/5TLT8A3.png)![accesstoken le userclaims alınması](https://i.imgur.com/iq2jfPK.png)
> #Bilgi 	 	
> userinfo endpoint
> 
> 	IdentityServer idtoken sub izni haricinde kimlik izinlerini içermez.
> Taki siz bu izni soruncaya dek. Bunun için client konfigürasyonu
> kısmında AlwaysIncludeUserClaimsInIdToken = true yapmalısınız. Fakat
> biz bunu yapmıyoruz. Aslında claimleri token içine gömebiliriz ama
> token size arttırmak istemiyoruz Dolayısıyla bu iş için identityserver
> içinde kullanabileceğimiz userinfo endpointi mevcut. Buraya bir access
> token ile istek atıp kullanıcıya ait gerekli izinleri alabiliriz. Eğer
> kullanıcıbilgi (userifo) endpointinden kullanıcı ile ilgili bilgi
> almak istiyorsanız token içinde profileid scope olmak zorundadır. 
>     Eğer authorization code + pkce akışımıza geri dönersek code ile token endpointine istek attığımızda bize id_token ile beraber
> access_token döner. İşte biz bu access_token ile userinfo endpointine
> ilgili kullanıcı için istekte bulunuruz. İşte bu noktada access_token
> identityserver tarafından validate edildikten sonra bize kullanıcı ile
> ilgili izinleri (user_claims) döndürür.

 
09.06.2020 15:26 Şimdi client app'ımızın StartUp.cs içinde bulunan OpenIdConnect nesnemize bu özelliği aktive ediyoruz. 

    options.GetClaimsFromUserInfoEndpoint = true;
Şimdi uygulama tekrar çalıştırıldığında claimlerin geldiği görülecektir.
![enter image description here](https://i.imgur.com/Str2Mpm.png)
Gereksiz claim tiplerini filtrelemek için StartUp.cs içinde constructor kısmına 

     JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); 

 kodunu ekliyoruz. Böylece yukarıdaki http şemaya sahip claimler gelmeyecektir. Ancak orada yine gereksiz claimler bulunmaktadır. Bunları filtrelemek içinde 
 ![enter image description here](https://i.imgur.com/6m1VJQM.png)
09.06.2020 16:07 Şimdi mevcut kullanıcımızın diğer özel izinlerine ihtiyacımız varsa yapacağımız  işlem, IdentityServer app üzerinde bulunan config.cs'ye gitmektir.  Burada adres ile ilgili bir izine ihtiyacımız varsa

      public static IEnumerable<IdentityResource> Ids =>
                //Scopeları kapsıyor
                //Bu scopelar ile identitytoken ile ilgili claimler çekilir.  
                new IdentityResource[]
                {
    
                    new IdentityResources.OpenId(),               
                    new IdentityResources.Profile(),
                    //Eğer kullanıcının adres bilgisini istiyorsak
                    new IdentityResources.Address()
                };
Adres claimi eklenir. Sonra clients içindeki scope alanına bu bilgi eklenir.
![enter image description here](https://i.imgur.com/tYiYE3s.png)Ayrıca ilgili kullanıcının claimleri arasında addres bilgisi olması gerekir.
![enter image description here](https://i.imgur.com/1kNEm8n.png)Daha sonra ilgili client üzerinde startup.cs içinde ilgili scope eklenir
![enter image description here](https://i.imgur.com/Ni4zW18.png)Şimdi bu kodu çalıştırdığımızda identityserver ekranında scope gelmesine
rağmen  client tarafında bu claim gözükmeyecektir. Bunu çağırmak için custom bir request oluşturacağız.

Bu işlem için client projesine nuget üzerinden identitymodel kütüphanesi eklenir.  Bu kütüphane identityserver yazan kişiler tarafından oluşturulmuştur. Bu kütüphane ile identityserver üzerindeki endpointlere istediğimiz gibi isteklerde bulunabiliriz.  Bunun için identityserver sunucumuza istekte bulunabilmek için startup.cs içinde httpclient nesnesi oluşturuyoruz.
![enter image description here](https://i.imgur.com/jy70x1H.png)Sonra bu claimi çağıracağımız actionı yazıyoruz
![enter image description here](https://i.imgur.com/KPPqkpH.png)Sıra geldi rol bazlı kullanıcı hareketlere, malum erişim izni verme sizin kim olduğunuzu söyler yetkilendirme ise ne yapabileceğinizi belirler.
Şimdi kullanıcılarımıza rol tanımlayalım
![enter image description here](https://i.imgur.com/h8i8grC.png)
Sonra bu role uygun identity resource tanımlayalım. Bu rol için önden tanımlanmış bir tip olmadığı için kendimiz custom identity resource tanımlayacağız. Sonra bu identiy resource ilgili client için scope olarak ekleyeceğiz
![enter image description here](https://i.imgur.com/AGfpCnE.png)
Bu işlemden sonra client app içindeki startup.cs'yi açıp roles scope register ediyoruz
![enter image description here](https://i.imgur.com/6wWrFWD.png)Bu noktada uygulamayı çalıştırırsak role claim'inin gelmediğini göreceğiz. Çünkü eklediğimiz bu claim build-in olmadığı için maplenmemiş durumdadır. İhtiyacımız bu claimi maplemektir. Bunun için aşağıdaki kod satırını startup.cs içine ekliyoruz.

     options.ClaimActions.MapUniqueJsonKey("role", "role");
daha sonra  UI kısmıda navigasyona rol kontrolü ekliyoruz
![enter image description here](https://i.imgur.com/mAgFaUW.png)Bu şekilde rolü çalıştırsak rol claimi gelse bile kullanıcının bu rolde olup olmadığını belirleyemiyoruz. Kullanıcının rolde olup olmadığını tokenvalidationparameters özelliği ile tespit edeceğiz.
![enter image description here](https://i.imgur.com/Yqgfl0F.png)Bu kodu çalıştırdığımızda 2 farklı kullanıcı için  user.isinrole metodunun çalıştığını göreceğiz. Burada rol kontrolü yapsak bile o rolde olmayan bir kullanıcı eğer sayfayı dışarıdan çağırsa güvenlik açığı olacaktır. Bunu engellemek için,  ilgili action tepesine aşağıdaki attribute ekliyoruz

     [Authorize(Roles ="payinguser")]
Eğer birden fazla rol ekleyeceksek rolleri , ile ayırabiliriz rol1,rol2 gibi

11.06.2020 15:08 
**API tarafı**
Authorizatin için burada code + pkce kullanılıyor
![enter image description here](https://i.imgur.com/pJpHfoM.png)
![enter image description here](https://i.imgur.com/jjHyvcf.png)![enter image description here](https://i.imgur.com/LaRzKsG.png)
İlk olarak identityserver config.cs içinde ilgili api için apiresource tanımlıyoruz

![enter image description here](https://i.imgur.com/qpWwBDn.png)Daha sonra bu resource kullanacak olan client'in scope alanına ekliyoruz
![enter image description here](https://i.imgur.com/bBzaEZ5.png)Startup.cs tarafında kullandığınız resource tipine göre 

      .AddInMemoryApiResources(Config.Apis)

  services.IdentityServer() kısmına dahil ediyoruz.
  Daha sonra Client uygulamamızın startup.cs kısmında bulunan ayarlarımıza api erişim scope'unu ekliyoruz
  
![enter image description here](https://i.imgur.com/Ekvg374.png)
Şimdi api tarafına geçiyoruz ve ilk yapmamız gereken işlem nuget'i açıp identityserver4.accesstokenvalidation kütüphanesini projeye eklemek olacak
![enter image description here](https://i.imgur.com/JIjPxgX.png)
Bu yükleme işleminden sonra identityserver middleware'ini startup.cs içinde kayıt etmemiz gerekiyor. Aşağıdaki kodu configuration metodunun içine ekliyoruz

![enter image description here](https://i.imgur.com/Kalo4H2.png)![enter image description here](https://i.imgur.com/bkEn1WH.png)Daha sonra api'de ilgili controller'ın class attribute olarak
   `[Authorize]` ekleyebiliriz.

Client uygulamamız api'ye erişirken access token'a ihtiyacı olacak. Bunun için bearer token handle eden bir handlerı projeye ekliyoruz.

![enter image description here](https://i.imgur.com/kuZqiS7.png)
Yazdığımız handleri startup.cs içinde register ediyoruz.
![enter image description here](https://i.imgur.com/ldhGEyJ.png)
11.06.2020 15:51  Uygulamayı çalıştırdıktan sonra kişiye ait resimleri getirmek için claim dediğimiz izinleri kullanıyoruz. ![enter image description here](https://i.imgur.com/4TcDH72.png)
Şimdi belki bu şekilde claim almak kolay gibi gözükebilir. Ancak kişiye ait diğer claimler nereden gelecek? UserInfoEndpointe istek atabiliriz ama bu yavaş ve maliyetli olacaktır. Onun yerine api resource içinde istediğimiz claimleri tanımlayacağız

![enter image description here](https://i.imgur.com/QteuXT3.png)Eğer bu noktada access token alıp jwt.io içine yapıştırsak kullanıcıya ait rolün geldiğini görebiliriz.

![enter image description here](https://i.imgur.com/nvyXUDY.png)Daha sonra buradaki güvenliği web client uygulamamız içinde yapıyoruz.
![enter image description here](https://i.imgur.com/N4QpPTS.png)
ve UI tarafında menü güvenliği için  ilgili menüyü rol kapsamına alıyoruz
![enter image description here](https://i.imgur.com/LHQ5YIe.png)**Yetkilendirme Politikaları**
